package test.com.jogodosanimais;

import org.junit.Test;

import com.jogodosanimais.JogoDosAnimais;

public class JogoDosAnimaisTest {

	@Test(expected = NullPointerException.class)
	public void iniciateTestWithNoInstaceOfSteps() {
		JogoDosAnimais.iniciate();
	}

}
