package com.jogodosanimais.utils;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.jogodosanimais.JogoDosAnimais;

/**
 * Classe de utilidades para o JPanel e Frame
 * 
 * @author Leandro Hoffmann
 * 
 */

public class Utils extends JogoDosAnimais {

	private static JFrame frame;

	public Utils() {
		createPanel();
	}

	public static Utils getInstance() {
		return new Utils();
	}

	/**
	 * Cria o painel principal
	 */
	private static void createPanel() {
		frame = new JFrame("Animais");
		frame.setSize(230, 110);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);

		JPanel panel = new JPanel();
		panel.setBorder(new EmptyBorder(10, 10, 10, 10));
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		JLabel text = new JLabel("Pense em um animal");
		text.setAlignmentX(Component.CENTER_ALIGNMENT);

		JButton ok = new JButton("Ok");
		ok.setAlignmentX(Component.CENTER_ALIGNMENT);
		ok.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				iniciate();
			}
		});
		panel.add(text);
		panel.add(ok);
		frame.add(panel);
		frame.setVisible(true);
	}

	/**
	 * Gera a pergunta e retorna o valor da resposta
	 * 
	 * @param pergunta
	 * @return resposta
	 */
	public static int pergunta(String pergunta) {
		return JOptionPane.showConfirmDialog(frame, pergunta, "Jogo do Animais", JOptionPane.YES_NO_OPTION);
	}

	/**
	 * Gera um popup indicando o acerto
	 */
	public static void acertei() {
		JOptionPane.showMessageDialog(frame, "Acertei de novo!", "Jogo dos Animais", JOptionPane.OK_OPTION);
	}
}
