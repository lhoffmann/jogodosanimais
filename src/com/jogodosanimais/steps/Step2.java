package com.jogodosanimais.steps;

import java.util.Iterator;

import javax.swing.JOptionPane;

import com.jogodosanimais.JogoDosAnimais;
import com.jogodosanimais.object.Animal;
import com.jogodosanimais.utils.Utils;

/**
 * Step que verifica qual animal foi pensado e se ele possui tal caracteristica
 * 
 * @author Leandro Hoffmann
 * 
 */

public class Step2 extends JogoDosAnimais {

	public static Step2 getInstance() {
		return new Step2();
	}

	/**
	 * Metodo que verifica se o animal possui tal caracteristica, caso termine a
	 * lista, pergunta se é Macaco
	 */
	public void eacao() {
		Iterator<Animal> iter = animais.iterator();
		while (iter.hasNext()) {
			Animal animal = iter.next();
			int ret = Utils.pergunta("O animal que você pensou " + ((animal.getAcao() == null) ? "" : animal.getAcao()) + "?");
			if (ret == JOptionPane.YES_OPTION) {
				eanimal(animal.getNome());
				break;
			}
			if (!iter.hasNext()) {
				eanimal("Macaco");
				break;
			}
		}
	}

	/**
	 * Metodo que verifica qual o animal pensado
	 */
	public void eanimal(String nome) {
		int ret = Utils.pergunta("O animal que você pensou é um " + ((nome == null) ? "" : nome) + "?");
		if (ret == JOptionPane.YES_OPTION) {
			Utils.acertei();
		} else {
			novoAnimal.qualAnimal(nome);
		}
	}
}