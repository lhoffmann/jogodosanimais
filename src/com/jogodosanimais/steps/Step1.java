package com.jogodosanimais.steps;

import javax.swing.JOptionPane;

import com.jogodosanimais.JogoDosAnimais;
import com.jogodosanimais.utils.Utils;

/**
 * Step que inicia perguntando se é Tubarão
 * 
 * @author Leandro Hoffmann
 * 
 */

public class Step1 extends JogoDosAnimais {

	public static Step1 getInstance() {
		return new Step1();
	}

	public void etubarao() {

		int ret = Utils.pergunta("O animal que você pensou é um Tubarão?");
		if (ret == JOptionPane.YES_OPTION) {
			Utils.acertei();
		} else {
			novoAnimal.qualAnimal("Tubarão");
		}

	}
}