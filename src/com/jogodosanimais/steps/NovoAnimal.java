package com.jogodosanimais.steps;

import javax.swing.JOptionPane;

import com.jogodosanimais.JogoDosAnimais;
import com.jogodosanimais.object.Animal;

/**
 * Claase para criação de um novo animal
 * 
 * @author Leandro Hoffmann
 * 
 */

public class NovoAnimal extends JogoDosAnimais {

	public static NovoAnimal getInstance() {
		return new NovoAnimal();
	}

	/**
	 * Metodo sem validação, para seguir o requisito que deve ter o mesmo
	 * comportamento do original
	 * 
	 * @param nome
	 */
	public void qualAnimal(String nome) {
		String animal = JOptionPane.showInputDialog("Qual Animal você pensou?");
		String acao = JOptionPane.showInputDialog("Um(a)" + ((animal == null) ? "" : animal) + " ____ mas um(a) " + nome + " não.");
		getAnimais().add(new Animal(animal, acao));
	}
	/**
	 * Metodo com validação dos nomes dos animais
	 * 
	 * @param nome
	 */

	// public void qualAnimal(String nome) {
	// String animal = null;
	// String acao = null;
	// while (animal == null || animal.isEmpty()) {
	// animal = JOptionPane.showInputDialog("Qual Animal você pensou?");
	// }
	// while (acao == null || acao.isEmpty()) {
	// acao = JOptionPane.showInputDialog("Um(a)" + animal + " ____ mas um(a) "
	// + nome + " não.");
	// }
	// getAnimais().add(new Animal(animal, acao));
	// }
}