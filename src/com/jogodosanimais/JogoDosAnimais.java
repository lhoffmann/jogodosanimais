package com.jogodosanimais;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import com.jogodosanimais.object.Animal;
import com.jogodosanimais.steps.NovoAnimal;
import com.jogodosanimais.steps.Step1;
import com.jogodosanimais.steps.Step2;
import com.jogodosanimais.utils.Utils;

/**
 * Jogo dos Animais, teste para VejoAoVivo. Classe que contem o Main e instancia
 * as utilidades e steps
 * 
 * @author Leandro Hoffmann
 * 
 */

public class JogoDosAnimais {

	public static ArrayList<Animal> animais;
	public static Step1 step1;
	public static Step2 step2;
	public static NovoAnimal novoAnimal;
	public static Utils utils;

	public static void main(String[] args) {
		getInstance();
	}

	private static void getInstance() {
		utils = Utils.getInstance();
		step1 = Step1.getInstance();
		step2 = Step2.getInstance();
		novoAnimal = NovoAnimal.getInstance();
	}

	public static void iniciate() {
		int ret = Utils.pergunta("O animal que você pensou vive na água?");
		if (ret == JOptionPane.YES_OPTION) {
			step1.etubarao();
		} else {
			if (getAnimais().isEmpty()) {
				step2.eanimal("Macaco");
			} else {
				step2.eacao();
			}
		}
	}

	public static ArrayList<Animal> getAnimais() {
		if (animais == null) {
			animais = new ArrayList<Animal>();
		}
		return animais;
	}
}